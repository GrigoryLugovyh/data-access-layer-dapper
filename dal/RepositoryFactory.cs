﻿using System;

namespace dal
{
    public interface IRepositoryFactory
    {
        IRepository<TEntity> Create<TEntity>(IDbContext context) where TEntity : Entity;
    }

    public class RepositoryFactory : IRepositoryFactory
    {
        public IRepository<TEntity> Create<TEntity>(IDbContext context) where TEntity : Entity
        {
            return (IRepository<TEntity>) Activator.CreateInstance(typeof(IRepository<TEntity>), context);
        }
    }
}