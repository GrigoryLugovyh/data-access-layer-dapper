﻿namespace dal.Example
{
    public class BankRepository : Repository<Bank>
    {
        public BankRepository(IDbContext context) : base(context)
        {
        }
    }
}