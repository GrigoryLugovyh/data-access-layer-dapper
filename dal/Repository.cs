﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper.Contrib.Extensions;

namespace dal
{
    public interface IRepository
    {
    }

    public interface IRepository<TEntity> : IRepository where TEntity : Entity
    {
        Task<int> Create(TEntity entity, IDbTransaction transaction = null, int? commandTimeoutSecs = null);

        Task<TEntity> ReadById(int id, IDbTransaction transaction = null, int? commandTimeoutSecs = null);

        Task<IEnumerable<TEntity>> ReadAll(IDbTransaction transaction = null, int? commandTimeoutSecs = null);

        Task<bool> Update(TEntity entity, IDbTransaction transaction = null, int? commandTimeoutSecs = null);

        Task<bool> Delete(TEntity entity, IDbTransaction transaction = null, int? commandTimeoutSecs = null);

        Task<bool> DeleteAll(IDbTransaction transaction = null, int? commandTimeoutSecs = null);
    }

    public class Repository<TEntity> : IRepository<TEntity> where TEntity : Entity
    {
        private readonly IDbContext _context;

        public Repository(IDbContext context)
        {
            _context = context;
        }

        public virtual async Task<int> Create(TEntity entity, IDbTransaction transaction = null,
            int? commandTimeoutSecs = null)
        {
            return await Invoke(connection => connection.InsertAsync(entity, transaction, commandTimeoutSecs));
        }

        public virtual async Task<TEntity> ReadById(int id, IDbTransaction transaction = null,
            int? commandTimeoutSecs = null)
        {
            return await Invoke(connection => connection.GetAsync<TEntity>(id, transaction, commandTimeoutSecs));
        }

        public async Task<IEnumerable<TEntity>> ReadAll(IDbTransaction transaction = null,
            int? commandTimeoutSecs = null)
        {
            return await Invoke(connection => connection.GetAllAsync<TEntity>(transaction, commandTimeoutSecs));
        }

        public virtual async Task<bool> Update(TEntity entity, IDbTransaction transaction = null,
            int? commandTimeoutSecs = null)
        {
            return await Invoke(connection => connection.UpdateAsync(entity, transaction, commandTimeoutSecs));
        }

        public virtual async Task<bool> Delete(TEntity entity, IDbTransaction transaction = null,
            int? commandTimeoutSecs = null)
        {
            return await Invoke(connection => connection.DeleteAsync(entity, transaction, commandTimeoutSecs));
        }

        public virtual async Task<bool> DeleteAll(IDbTransaction transaction = null, int? commandTimeoutSecs = null)
        {
            return await Invoke(connection => connection.DeleteAllAsync<TEntity>(transaction, commandTimeoutSecs));
        }

        protected async Task<TOutValue> Invoke<TOutValue>(Func<IDbConnection, Task<TOutValue>> request)
        {
            var connection = await _context.GetConnection();
            return await request(connection);
        }
    }
}