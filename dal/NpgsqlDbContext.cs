﻿using System;
using System.Data;
using System.Threading.Tasks;
using Npgsql;

namespace dal
{
    public interface IDbContext : IDisposable
    {
        Task<IDbConnection> GetConnection();
    }

    public class NpgsqlDbContext : IDbContext
    {
        private readonly IApplicationSettings _applicationSettings;
        private NpgsqlConnection _contextConnection;

        public NpgsqlDbContext(IApplicationSettings applicationSettings)
        {
            _applicationSettings = applicationSettings;
        }

        public async Task<IDbConnection> GetConnection()
        {
            if (_contextConnection == null)
            {
                _contextConnection =
                    new NpgsqlConnection(_applicationSettings.GetSetting("topology/config/connection_string"));
                await _contextConnection.OpenAsync();
            }

            return _contextConnection;
        }

        public void Dispose()
        {
            if(_contextConnection != null && _contextConnection.State == ConnectionState.Open)
                _contextConnection.Close();
        }
    }
}