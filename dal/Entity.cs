﻿using Dapper.Contrib.Extensions;

namespace dal
{
    public class Entity
    {
        [Key]
        public int Id { get; set; }
    }
}