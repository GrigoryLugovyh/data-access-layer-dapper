﻿using System;
using System.Collections.Concurrent;
using System.Data;
using System.Threading.Tasks;

namespace dal
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<TEntity> GetRepository<TEntity>() where TEntity : Entity;
        Task<IDbTransaction> BeginTransaction();
        void CommitTransaction();
        void RollbackTransaction();
    }

    public class UnitOfWork : IUnitOfWork
    {
        private readonly IRepositoryFactory _repositoryFactory;
        private readonly IDbContext _context;
        private readonly ConcurrentDictionary<Type, IRepository> _repositories;
        private IDbTransaction _transaction;

        public UnitOfWork(IRepositoryFactory repositoryFactory, IDbContext context)
        {
            _repositoryFactory = repositoryFactory;
            _context = context;

            _repositories = new ConcurrentDictionary<Type, IRepository>();
        }

        public IRepository<TEntity> GetRepository<TEntity>() where TEntity : Entity
        {
            return (IRepository<TEntity>) _repositories.GetOrAdd(typeof(IRepository<TEntity>),
                t => _repositoryFactory.Create<TEntity>(_context));
        }

        public async Task<IDbTransaction> BeginTransaction()
        {
            return _transaction ?? (_transaction = (await _context.GetConnection()).BeginTransaction());
        }

        public void CommitTransaction()
        {
            if(_transaction == null) return;

            _transaction.Commit();
            _transaction.Dispose();
            _transaction = null;
        }

        public void RollbackTransaction()
        {
            if(_transaction == null) return;

            _transaction.Rollback();
            _transaction.Dispose();
            _transaction = null;
        }

        public void Dispose()
        {
            CommitTransaction();

            _context.Dispose();
        }
    }
}